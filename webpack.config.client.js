const webpack = require("webpack");
const path = require("path");

const devServerPort = 3001;
const isProd = process.env.NODE_ENV === "production";

module.exports = {
  mode: process.env.NODE_ENV,
  devtool: isProd ? false : "inline-source-map",
  entry: ["react-hot-loader/patch", "./client/src/index"],
  target: "web",
  resolve: {
    alias: {
      shared: path.resolve(__dirname, "shared"),
    },
  },
  module: {
    rules: [
      {
        test: [/\.gif$/, /\.jpe?g$/, /\.png$/],
        loader: "file-loader",
        options: {
          name: "[name]-[hash:8].[ext]",
          outputPath: "dist/images/",
        },
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: [/\.svg$/],
        loader: "react-svg-loader",
        options: {
          svgo: {
            plugins: [{ removeTitle: true }],
            floatPrecision: 2,
          },
        },
      },
      {
        test: /\.js?$/,
        use: "babel-loader",
        include: [
          path.join(__dirname, "client/src"),
          path.join(__dirname, "shared"),
        ],
      },
    ],
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
  ],
  devServer: {
    // Only used in development mode
    host: "localhost",
    port: devServerPort,
    historyApiFallback: true,
    hot: true,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers":
        "X-Requested-With, content-type, Authorization",
    },
  },
  output: {
    path: path.join(__dirname, "client/dist/assets/js"),
    publicPath: `http://localhost:${devServerPort}/`,
    filename: "bundle.js",
  },
};
