const express = require("express");

const app = express();

app.use(express.static(__dirname + "/dist"));

app.get("*", async (req, res) => {
  const isProduction = process.env.NODE_ENV === "production";

  const bundlePath = isProduction
    ? "/assets/js/bundle.js"
    : "http://localhost:3001/bundle.js";

  let html = `<!doctype html>
    <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="x-ua-compatible" content="ie=edge">
            <title>Automobile En Direct</title>
            <meta name="description" content="">
            <meta name="viewport"
            content="width=device-width,  initial-scale=1">
        </head>
        <body>
          <div id="root"></div>
          <script src="${bundlePath}"></script>
        </body>
    </html>`;

  res.send(html);
});

app.listen(3000, function () {
  console.log("App listening on port 3000!");
});
