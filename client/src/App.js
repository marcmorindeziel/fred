import React from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const App = () => {
  return (
    <div>
      <DatePicker />
    </div>
  );
};

export default App;
